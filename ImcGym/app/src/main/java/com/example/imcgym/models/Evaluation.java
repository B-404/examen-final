package com.example.imcgym.models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Evaluation implements Serializable {
    private long id;
    private Date dayDate;
    private Double Imc;
    private Double height;
    private Date due;


    public Evaluation(Date dayDate, Double imc, Double height,Date due) {
        this.dayDate = dayDate;
        Imc = (height/1.70*2);
        this.height = height;
        this.due = due;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDayDate() {
        return dayDate;
    }

    public Double getImc() {
        return Imc;
    }

    public Double getHeight() {
        return height;
    }

    public Date getDue() {
        return due;
    }


}
