package com.example.imcgym;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.imcgym.controllers.AuthController;
import com.example.imcgym.models.User;
import com.example.imcgym.ui.DatePickerFragment;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegisterActivity extends AppCompatActivity {


    private final String DATE_PATTERN = "yyyy-MM-dd";

    private Button btnLogin;
    private TextInputLayout tilBirthday;
    private TextInputLayout tilUsername;
    private TextInputLayout tilFirstname;
    private TextInputLayout tilLastname;
    private TextInputLayout tilPassword;
    private TextInputLayout tilHeight;
    private Button btnRegister;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        tilHeight = findViewById(R.id.activity_eregister_til_height);
        tilUsername = findViewById(R.id.activity_register_til_username);
        tilLastname = findViewById(R.id.activity_register_til_lastname);
        tilFirstname = findViewById(R.id.activity_register_til_firstname);
        tilPassword = findViewById(R.id.activity_register_til_password);
        btnLogin = findViewById(R.id.activity_register_btn_login);
        tilBirthday = findViewById(R.id.activity_register_til_birthday);
        btnRegister = findViewById(R.id.activity_register_btn_register);

        tilBirthday.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this,tilBirthday,new Date());
        });


        btnRegister.setOnClickListener(view -> {
            String username = tilUsername.getEditText().getText().toString();
            String firstname = tilFirstname.getEditText().getText().toString();
            String lastname = tilLastname.getEditText().getText().toString();
            String birthday = tilBirthday.getEditText().getText().toString();
            String height = tilHeight.getEditText().getText().toString();
            String password = tilPassword.getEditText().getText().toString();


            SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);

            boolean usernameValid = !username.isEmpty();
            boolean passwordValid = !password.isEmpty();
            boolean firstnamedValid = !firstname.isEmpty();
            boolean lastnameValid = !lastname.isEmpty();
            boolean birthdayValid = !birthday.isEmpty();
            boolean heightValid = !height.isEmpty();

            if(!usernameValid){
                tilUsername.setError("El usuario es incorrecto");
            }else{
                tilUsername.setError(null);
            }
            if(!firstnamedValid){
                tilFirstname.setError("Campo requerido");
            }else{
                tilFirstname.setError(null);
            }

            if(!lastnameValid){
                tilLastname.setError("Campo requerido");
            }else{
                tilLastname.setError(null);
            }

            if(!birthdayValid){
                tilBirthday.setError("Campo requerido");
            }else{
                tilBirthday.setError(null);
            }

            if(!heightValid){
                tilHeight.setError("Campo requerido");
            }else{
                tilHeight.setError(null);
            }

            if(!passwordValid){
                tilPassword.setError("Campo requerido");
            }else{
                tilPassword.setError(null);
            }


           Date birthdayDate = null;
           try{
              birthdayDate = dateFormatter.parse(birthday);
           }catch(ParseException e){
               e.printStackTrace();
           }

            double datoheight= 0.0;

           try{
             datoheight  = Double.parseDouble(height);
               tilHeight.setError(null);
               tilHeight.setErrorEnabled(false);
           }catch (Exception error){
               tilHeight.setError("La estatura es invalida Ej:1.7");
               return;
           }


            User user = new User (username,firstname,lastname,birthdayDate,datoheight);
            user.setPassword(password);

            AuthController controller = new AuthController(view.getContext());
            controller.register(user);

        });

         btnLogin.setOnClickListener(view -> {
             Intent i = new Intent(view.getContext(),MainActivity.class);
             startActivity(i);
             finish();
         });

    }
}