package com.example.imcgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.imcgym.controllers.AuthController;
import com.example.imcgym.models.User;
import com.google.android.material.textfield.TextInputLayout;


public class LoginActivity extends AppCompatActivity {

    private Button btnLogin;
    private Button btnRegister;
    private TextInputLayout tilUser;
    private TextInputLayout tilPassword;
    private AuthController authController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        authController = new AuthController(this);

        authController.checkUserSession();

        btnLogin = findViewById(R.id.activity_login_btn_login);
        btnRegister = findViewById(R.id.activity_login_btn_register);
        tilUser = findViewById(R.id.activity_login_til_user);
        tilPassword = findViewById(R.id.activity_login_til_password);

        btnRegister.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(),RegisterActivity.class);
            startActivity(i);
            finish();
        });

       btnLogin.setOnClickListener(view ->  {

               Toast.makeText(view.getContext(), "Iniciando Sesion", Toast.LENGTH_SHORT).show();

               String username = tilUser.getEditText().getText().toString();
               String password = tilPassword.getEditText().getText().toString();

               boolean userValid = !username.isEmpty();
               boolean passwordValid = !password.isEmpty();

               if(!userValid){
                  tilUser.setError("El usuario es incorrecto");
               }else{
                   tilUser.setError(null);
               }

               if(!passwordValid){
                   tilPassword.setError("Campo requerido");
               }else{
                   tilPassword.setError(null);
               }

               if(userValid && passwordValid){
                   authController.login(username,password);
               }else{
                   Toast.makeText(view.getContext(), "Campo Requerido", Toast.LENGTH_SHORT).show();
               }



       });


    }
}