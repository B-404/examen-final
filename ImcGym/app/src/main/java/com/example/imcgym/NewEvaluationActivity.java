package com.example.imcgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.telecom.Call;
import android.widget.Button;
import android.widget.Toast;

import com.example.imcgym.models.User;
import com.example.imcgym.ui.DatePickerFragment;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NewEvaluationActivity extends AppCompatActivity {

    private final String DATE_PATTERN = "yyyy-MM-dd";

    private TextInputLayout tilHeight;
    private TextInputLayout tilDate;
    private Button btnRegister;
    private Button btnBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_evaluation);

        tilHeight = findViewById(R.id.activity_newEvaluation_til_height);
        tilDate = findViewById(R.id.activity_newEvaluation_til_date);
        btnRegister = findViewById(R.id.activity_newEvaluation_btn_register);
        btnBack = findViewById(R.id.activity_newEvaluation_btn_back);


        tilDate.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this,tilDate,new Date());
        });

       btnRegister.setOnClickListener(view -> {

           String height = tilHeight.getEditText().getText().toString();
           String infoDate = tilDate.getEditText().getText().toString();

           SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);

           double datoheight= 0.0;

           try{
               datoheight  = Double.parseDouble(height);
               tilHeight.setError(null);
               tilHeight.setErrorEnabled(false);
           }catch (Exception error){
               tilHeight.setError("La estatura es invalida Ej:1.7");
               return;
           }

           Date birthdayDate = null;
           try{
               birthdayDate = dateFormatter.parse(infoDate);
           }catch(ParseException e){
               e.printStackTrace();
           }



       });
    }
}