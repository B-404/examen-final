package com.example.imcgym.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Date;
@Entity(tableName = "user",indices = {@Index(value = "user",unique = true)})
public class UserEntity implements IUser {
    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "user")
    private String username;

    @ColumnInfo(name = "first_name")
    private String firstname;

    @ColumnInfo(name = "last_name")
    private String lastname;

    @ColumnInfo(name = "birthday")
    private Date birthday;

    @ColumnInfo(name = "height")
    private Double height;

    @ColumnInfo(name = "password")
    private String password;

    public UserEntity(long id, String username, String firstname, String lastname, Date birthday, Double height, String password) {
        this.id = id;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.height = height;
        this.password = password;
    }

    public long getId() {
        return id;
    }



    public String getUsername() {
        return username;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Double getHeight() {
        return height;
    }

    public String getPassword() {
        return password;
    }


}

