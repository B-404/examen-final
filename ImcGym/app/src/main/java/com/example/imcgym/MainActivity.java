package com.example.imcgym;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.imcgym.controllers.AuthController;
import com.example.imcgym.models.Evaluation;
import com.example.imcgym.ui.DatePickerFragment;
import com.example.imcgym.ui.EvaluationAdapter;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextInputLayout tilFrom;
    private TextInputLayout tilTo;
    private ListView lvAllEvaluations;
    private Button btnlogout;
    private AuthController authController;
    private Button btnNewEvaluation;
    private Button btnFilter;

    private List<Evaluation> listEva = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        authController = new AuthController(this);

       btnNewEvaluation = findViewById(R.id.activity_main_btn_new_eva);
       btnFilter = findViewById(R.id.activity_main_btn_filter);
       tilFrom = findViewById(R.id.activity_main_til_from);
       tilTo = findViewById(R.id.activity_main_til_to);
       lvAllEvaluations = findViewById(R.id.activity_main_all_lv_ev);
       btnlogout = findViewById(R.id.activity_main_btn_logout);

       btnNewEvaluation.setOnClickListener(view -> {


           Intent i = new Intent(view.getContext(),NewEvaluationActivity.class);
           startActivity(i);
           finish();
       });

        tilFrom.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this,tilFrom,new Date());
        });

        tilTo.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this,tilTo,new Date());
        });



        for(int x = 0; x < 10; ++x){
            Evaluation newEvaluation = new Evaluation(new Date(),(70/1.70*2),1.70,new Date());
            newEvaluation.setId(x);
            listEva.add(newEvaluation);
        }




        EvaluationAdapter adapter = new EvaluationAdapter(this,listEva);


       lvAllEvaluations.setAdapter(adapter);


       lvAllEvaluations.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
           @Override
           public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {

               final int wich_item = position;

               new AlertDialog.Builder(MainActivity.this)
                       .setIcon(android.R.drawable.ic_delete)
                       .setTitle("Esta seguro de eliminar?")
                       .setMessage("Quieres eliminar la evaluacion?")
                       .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int wich) {
                               listEva.remove(wich_item);
                               adapter.notifyDataSetChanged();
                           }
                       })
                       .setNegativeButton("NO",null)
                       .show();

               return true;
           }
       });

       btnlogout.setOnClickListener(view -> {
           authController.logout();

       });
    }
}